const yaml = require('js-yaml')
const { readFileSync, writeFileSync } = require('fs')
const { resolve } = require('path')

const file = resolve(__dirname, '../src/script/content-data.yaml')
const doc = yaml.load(readFileSync(file))

doc.list.sort((a, b) => {
  if (a.title.toLowerCase() > b.title.toLowerCase()) return 1
  else if (a.title.toLowerCase() < b.title.toLowerCase()) return -1
  else return 0
})

writeFileSync(file, yaml.dump(doc))
