import { content } from './content'
import { clearContainer } from './utils'
import { SearchScreen } from './search-screen'
import { QuestionScreen } from './question-screen'
import { SearchRegionalScreen } from './search-regional-screen'

function initApp() {
  const searchScreen = new SearchScreen({
    content: content.list,
    openQuestionScreen: () => adjustVisibleHash('#question', 'launch'),
    openRegionalSearch: () => adjustVisibleHash('#searchregional', 'launch')
  })
  const questionScreen = new QuestionScreen(content.question)
  const searchRegionalScreen = new SearchRegionalScreen({ content: content.regionalLink })
  const container = document.getElementById('app')
  const baseLocation = location.protocol + '//' + location.host + location.pathname

  questionScreen.listener = {
    onAnswersUpdated: () => adjustVisibleHash('#question/' + questionScreen.selectedAnswers.join('/'), 'replace history item')
  }

  function setVisibleScreen(node: HTMLElement) {
    clearContainer(container); container.append(node)
  }

  let currentHash: string | null = null

  function adjustVisibleHash(newHash: string, type: 'replace history item' | 'add history item' | 'launch') {
    if (type === 'launch') openScreen(newHash)
    else currentHash = newHash

    if (type === 'replace history item') location.replace(baseLocation + newHash)
    else location.href = baseLocation + newHash
  }

  function openScreen(hash: string) {
    if (hash.indexOf('#search/') !== -1) {
      searchScreen.termChangeListener = () => {
        adjustVisibleHash('#search/' + encodeURIComponent(searchScreen.term), 'replace history item')
      }

      if (hash !== currentHash) {
        searchScreen.term = decodeURIComponent(hash.substr('#search/'.length))

        setVisibleScreen(searchScreen.root)
      }
    } else if (hash.indexOf('#question') === 0) {
      if (hash !== currentHash) {
        questionScreen.selectedAnswers = hash.substring('#question/'.length).split('/').filter((item) => item.length > 0).map((item) => {
          const parsed = parseInt(item)

          return isFinite(parsed) ? parsed : 0
        })

        setVisibleScreen(questionScreen.root)
      }
    } else if (hash.indexOf('#searchregional') === 0) {
      setVisibleScreen(searchRegionalScreen.root)
    } else {
      searchScreen.termChangeListener = () => {
        if (searchScreen.term !== '') {
          adjustVisibleHash('#search/' + encodeURIComponent(searchScreen.term), 'add history item')
        }
      }

      if (hash !== currentHash) {
        searchScreen.term = ''

        setVisibleScreen(searchScreen.root)
      }
    }

    currentHash = hash
  }; openScreen(location.hash)

  window.addEventListener('hashchange', () => openScreen(location.hash))
}

document.addEventListener('DOMContentLoaded', () => initApp())
