export class SearchEmptyView {
  readonly root = document.createElement('div')

  listener: null | (() => void) = null

  constructor() {
    this.root.classList.add('search-empty-view')

    const span = document.createElement('span')
    span.innerText = 'Nichts gefunden'

    const space = document.createElement('div')
    space.classList.add('space')

    const button = document.createElement('button')
    button.innerText = 'Fragebaum verwenden'

    this.root.append(span, space, button)

    button.addEventListener('click', () => {
      if (this.listener) this.listener()
    })
  }
}
