export class TitleBarBackIcon {
  readonly root = document.createElement('div')

  listener: (() => void) | null = null

  constructor() {
    this.root.classList.add('title-bar-back-icon')
    this.root.innerText = '<'

    this.root.addEventListener('click', () => {
      if (this.listener) this.listener()
    })
  }
}
