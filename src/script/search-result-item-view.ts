import { ContentListItem } from './content'

export class SearchResultItemView {
  readonly root = document.createElement('div')
  private readonly titleView = document.createElement('h3')
  private readonly textView = document.createElement('span')
  private readonly detailView = document.createElement('div')
  private readonly longTextView = document.createElement('p')
  private readonly imageContainer = document.createElement('div')
  private isExpanded: boolean = false
  private item: ContentListItem | null = null

  constructor() {
    this.root.classList.add('search-result')
    this.detailView.classList.add('search-result-details')
    this.imageContainer.classList.add('search-result-images')

    this.detailView.append(this.imageContainer, this.longTextView)
    this.root.append(this.titleView, this.textView)

    this.root.addEventListener('click', (e) => {
      e.preventDefault()

      this.expanded = !this.expanded
    })
  }

  set content(item: ContentListItem) {
    this.item = item
    this.titleView.innerText = item.title
    this.textView.innerText = item.text
    this.longTextView.innerText = item.detailText ?? 'Hier sind keine weiteren Details verfügbar'

    while (this.imageContainer.firstChild) this.imageContainer.firstChild.remove()
  }

  set expanded(value: boolean) {
    if (value) {
      this.root.append(this.detailView)
      this.root.classList.add('expand')
    } else {
      this.detailView.remove()
      this.root.classList.remove('expand')
    }

    if (value && !this.imageContainer.firstChild) {
      if (this.item && this.item.image) {
        this.item.image.forEach((image) => {
          const view = document.createElement('img')

          view.src = image

          this.imageContainer.append(view)
        })
      }
    }

    this.isExpanded = value
  }

  get expanded() { return this.isExpanded }
}
