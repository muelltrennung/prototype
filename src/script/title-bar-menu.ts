export class TitleBarMenu {
  readonly root = document.createElement('div')
  private readonly contentRoot = document.createElement('div')

  constructor() {
    this.root.classList.add('title-bar-menu')
    this.contentRoot.classList.add('content')

    this.root.append(this.contentRoot)
    this.root.addEventListener('click', () => this.visible = false)
  }

  addOption({ text, listener }: { text: string, listener: () => void }) {
    const view = document.createElement('a')

    view.innerText = text
    view.href = '#'
    view.addEventListener('click', (e) => { e.preventDefault(); listener() })

    this.contentRoot.append(view)
  }

  set visible(enable: boolean) {
    if (enable) this.root.classList.add('show')
    else this.root.classList.remove('show')
  }
}
