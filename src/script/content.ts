export interface ContentSchema {
  list: ContentListItems
  question: QuestionItems
  regionalLink: RegionalLinkItems
}

export interface ContentListItem {
  title: string
  text: string
  detailText?: string
  image?: Array<string>
}

export type ContentListItems = Array<ContentListItem>

export interface QuestionItem {
  text: string
  options: OptionItems
}

export type QuestionItems = {[key: string]: QuestionItem} & { start: QuestionItem }

export type OptionItem = {
  text: string
  nextQuestionId?: string
  result?: string
  image?: string
}

export type OptionItems = Array<OptionItem>

export interface RegionalLinkItem {
  postalCode: Array<string>
  url: string
}

export type RegionalLinkItems = Array<RegionalLinkItem>

declare const require: (path: string) => object
export const content = require('./content-data.yaml') as ContentSchema
