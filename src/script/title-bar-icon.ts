export class TitleBarIcon {
  readonly root = document.createElement('div')
  listener: (() => void) | null = null

  constructor() {
    this.root.classList.add('title-bar-icon')

    this.root.append(...[0, 1, 2].map(() => document.createElement('div')))

    this.root.addEventListener('click', (e) => {
      e.preventDefault()

      if (this.listener !== null) this.listener()
    })
  }
}
