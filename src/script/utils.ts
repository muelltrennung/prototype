export function clearContainer(container: HTMLElement) {
  while (container.lastChild) container.removeChild(container.lastChild)
}
