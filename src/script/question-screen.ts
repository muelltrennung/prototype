import { TitleBar } from './title-bar'
import { QuestionItems } from './content'
import { QuestionCard } from './question-card'
import { QuestionResultCard } from './question-result-card'

interface QuestionScreenListener {
  onAnswersUpdated: () => void
}

export class QuestionScreen {
  readonly root = document.createElement('div')
  private readonly contentContainer = document.createElement('div')
  private readonly titleBar = new TitleBar()
  private readonly data: QuestionItems
  private selectedAnswersInternal: Array<number> = []
  private questionCards: Array<QuestionCard> = []
  private questionIds: Array<string> = []
  private resultCard: QuestionResultCard | null = null

  listener: QuestionScreenListener | null = null

  constructor(data: QuestionItems) {
    this.data = data

    this.root.classList.add('screen')
    this.contentContainer.classList.add('question-screen-items')

    this.titleBar.title = 'Mülltrennung'
    this.titleBar.setupBackButton(() => history.back())

    this.root.append(this.titleBar.root, this.contentContainer)

    this.appendQuestionCard('start')
  }

  get selectedAnswers(): Array<number> { return [...this.selectedAnswersInternal] }

  set selectedAnswers(value: Array<number>) {
    value.forEach((option, index) => {
      if (this.selectQuestionOption(index, option)) {
        this.questionCards[index].selectRadioButton(option)
      }
    })
  }

  private appendQuestionCard(id: string) {
    const selfIndex = this.questionCards.length
    const question = this.data[id]
    const card = new QuestionCard(selfIndex)

    card.content = question

    card.listener = {
      onAnswerSelected: (answerIndex) => {
        this.selectQuestionOption(selfIndex, answerIndex)

        if (this.listener) this.listener.onAnswersUpdated()
      }
    }

    this.questionCards.push(card)
    this.questionIds.push(id)
    this.contentContainer.appendChild(card.root)
  }

  private selectQuestionOption(questionIndex: number, answerIndex: number): boolean {
    const question = this.data[this.questionIds[questionIndex]]; if (!question) return false
    const answer = question.options[answerIndex]; if (!answer) return false

    this.dropResultCard()
    while (this.questionCards.length > questionIndex + 1) this.dropLastQuestionCard()
    while (this.selectedAnswersInternal.length > questionIndex) this.selectedAnswersInternal.pop()

    this.selectedAnswersInternal.push(answerIndex)

    if (typeof answer.nextQuestionId === 'string') {
      this.appendQuestionCard(answer.nextQuestionId)
      this.scrollToEnd()
    } else if (typeof answer.result === 'string') {
      this.resultCard = new QuestionResultCard()

      this.resultCard.content = answer.result

      this.contentContainer.appendChild(this.resultCard.root)
      this.scrollToEnd()
    }

    return true
  }

  private dropLastQuestionCard() {
    this.questionIds.pop()
    this.contentContainer.removeChild(this.questionCards.pop().root)
  }

  private dropResultCard() {
    if (this.resultCard !== null) {
      this.contentContainer.removeChild(this.resultCard.root)

      this.resultCard = null
    }
  }

  private scrollToEnd() {
    this.contentContainer.scrollTo({
      top: this.contentContainer.scrollHeight,
      behavior: 'smooth'
    })
  }
}
