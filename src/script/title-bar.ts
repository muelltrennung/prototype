import { TitleBarMenu } from './title-bar-menu'
import { TitleBarIcon } from './title-bar-icon'
import { TitleBarBackIcon } from './title-bar-back-icon'

export class TitleBar {
  readonly root = document.createElement('div')
  private readonly titleView = document.createElement('span')
  private readonly icon = new TitleBarIcon()
  private readonly menu = new TitleBarMenu()
  private readonly backButton = new TitleBarBackIcon()

  constructor() {
    this.root.classList.add('title-bar')

    this.root.append(this.titleView, this.menu.root)

    this.icon.listener = () => this.menu.visible = true
  }

  addOption({ text, listener }: { text: string, listener: () => void }) {
    this.menu.addOption({
      text,
      listener: () => {
        this.menu.visible = false

        listener()
      }
    })

    this.root.append(this.icon.root)
  }

  setupBackButton(listener: () => void) {
    this.backButton.listener = listener
    this.root.prepend(this.backButton.root)
  }

  set title(value: string) {
    this.titleView.innerText = value
  }
}
