import { RegionalLinkItems } from './content'
import { TitleBar } from './title-bar'

export class SearchRegionalScreen {
  readonly root = document.createElement('div')
  private readonly index: {[key: string]: string} = {}
  private readonly titleBar = new TitleBar()
  private readonly field = document.createElement('input')
  private readonly resultArea = document.createElement('div')
  private readonly waitingForInputResult = document.createElement('div')
  private readonly showResult = document.createElement('div')
  private readonly noResult = document.createElement('div')
  private readonly resultLink = document.createElement('a')

  constructor({ content }: { content: RegionalLinkItems }) {
    content.forEach((item) => {
      item.postalCode.forEach((postalCode) => this.index[postalCode] = item.url)
    })

    this.root.classList.add('screen')
    this.root.classList.add('search-regional-screen')

    this.resultArea.classList.add('result-area')

    this.titleBar.title = 'regionale Abfall-Suche'
    this.titleBar.setupBackButton(() => history.back())

    this.field.placeholder = '12345'
    this.field.maxLength = 5
    this.field.minLength = 5
    this.field.type = 'number'
    this.field.autofocus = true
    this.field.addEventListener('input', () => this.doSearch())

    this.waitingForInputResult.innerText = 'Bitte gebe eine Postleitzahl ein. Postleitzahlen bestehen immer aus genau fünf Ziffern.'

    this.resultLink.innerText = 'Hier'
    this.resultLink.target = '_blank'
    this.showResult.append(
      document.createTextNode('Für Deine Region haben wir etwas: '),
      this.resultLink,
      document.createTextNode(' geht es zu den Informationen des regionalen Abfallentsorgers')
    )

    this.noResult.innerText = 'Für Deine Region haben wir leider keine genaueren Informationen.'

    this.root.append(this.titleBar.root, this.field, this.resultArea)

    this.doSearch()
  }

  private doSearch() {
    const term = this.field.value

    if (!term.match(/\d{5}/)) {
      this.setResult(this.waitingForInputResult)

      return
    }

    const result = this.index[term]

    if (result) {
      this.resultLink.href = result

      this.setResult(this.showResult)
    } else {
      this.setResult(this.noResult)
    }
  }

  private setResult(view: HTMLElement) {
    while (this.resultArea.lastChild) this.resultArea.lastChild.remove()

    this.resultArea.appendChild(view)
  }
}
