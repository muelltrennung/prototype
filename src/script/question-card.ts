import { QuestionItem } from './content'
import { clearContainer } from './utils'
import { QuestionOptionView } from './question-option-view'

type QuestionCardListener = {
  onAnswerSelected: (index: number) => void
}

export class QuestionCard {
  listener: QuestionCardListener | null = null

  readonly root = document.createElement('div')
  private readonly title = document.createElement('h2')
  private readonly options = document.createElement('div')
  private readonly index: number
  private views: Array<QuestionOptionView> = []

  constructor(index: number) {
    this.index = index

    this.root.classList.add('question-card')

    this.root.appendChild(this.title)
    this.root.appendChild(this.options)
  }

  set content(item: QuestionItem) {
    this.title.innerText = item.text

    this.views = []; clearContainer(this.options)

    item.options.forEach((option, answerIndex) => {
      const view = new QuestionOptionView(this.index, answerIndex)

      view.item = option
      view.listener = () => {
        if (this.listener) this.listener.onAnswerSelected(answerIndex)
      }

      this.views.push(view)
      this.options.appendChild(view.root)
    })
  }

  selectRadioButton(index: number) { this.views[index].selected = true }
}
