import { TitleBar } from './title-bar'
import { SearchField } from './search-field'
import { ContentListItems } from './content'
import { SearchResultItemView } from './search-result-item-view'
import { SearchResultListView } from './search-result-list-view'
import { SearchEmptyView } from './search-empty-view'

type Listener = null | (() => void)

export class SearchScreen {
  readonly root = document.createElement('div')
  private readonly titleBar = new TitleBar()
  private readonly searchField = new SearchField()
  private readonly resultView = new SearchResultListView(() => new SearchResultItemView())
  private readonly searchEmptyView = new SearchEmptyView()
  private readonly content: ContentListItems

  termChangeListener: Listener = null

  get term() { return this.searchField.value }
  set term(value: string) { this.searchField.value = value }

  constructor({ content, openQuestionScreen, openRegionalSearch }: {
    content: ContentListItems
    openQuestionScreen: () => void
    openRegionalSearch: () => void
  }) {
    this.content = content

    this.root.classList.add('screen')

    this.root.append(this.titleBar.root)
    this.titleBar.title = 'Mülltrennung'
    this.titleBar.addOption({ text: 'Fragebaum', listener: openQuestionScreen })
    this.titleBar.addOption({ text: 'Über das Projekt', listener: () => window.open('https://muelltrennung.gitlab.io/')})
    this.titleBar.addOption({ text: 'regionale Informationen', listener: openRegionalSearch })

    this.root.append(this.searchField.root)
    this.root.append(this.resultView.root)

    this.updateContent()

    this.searchField.listener = () => {
      this.updateContent()

      if (this.termChangeListener !== null) this.termChangeListener()
    }

    this.searchEmptyView.listener = openQuestionScreen
  }

  private updateContent() {
    const term = this.searchField.value.toLowerCase().trim()

    const visibleItems = this.content.filter((item) => {
      return term.length === 0 || item.title.toLowerCase().indexOf(term) !== -1
    })

    this.resultView.setContent(visibleItems)

    if (visibleItems.length === 0) {
      if (!this.searchEmptyView.root.parentNode) {
        this.resultView.root.remove()
        this.root.append(this.searchEmptyView.root)
      }
    } else {
      if (!this.resultView.root.parentNode) {
        this.searchEmptyView.root.remove()
        this.root.append(this.resultView.root)
      }
    }
  }
}
