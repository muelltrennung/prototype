export class QuestionResultCard {
  readonly root = document.createElement('div')

  set content(text: string) {
    this.root.innerText = text
  }
}
