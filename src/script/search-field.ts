type Listener = null | (() => void)

export class SearchField {
  readonly root = document.createElement('input')
  listener: Listener = null

  constructor() {
    this.root.classList.add('search-field')
    this.root.placeholder = 'Suchbegriff eingeben'
    this.root.autofocus = true
    this.root.addEventListener('input', () => { if (this.listener) this.listener() })
  }

  get value(): string { return this.root.value }
  set value(newValue: string) {
    if (newValue === this.value) return

    this.root.value = newValue

    if (this.listener) this.listener()
  }
}
