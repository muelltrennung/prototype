import { clearContainer } from './utils'

type AbstractSearchResultItemView<T> = { content: T, root: HTMLElement }

export class SearchResultListView<T> {
  readonly root = document.createElement('div')
  private readonly createItemView: () => AbstractSearchResultItemView<T>

  constructor(createItemView: () => AbstractSearchResultItemView<T>) {
    this.createItemView = createItemView

    this.root.classList.add('search-result-list')
  }

  setContent(items: Array<T>) {
    clearContainer(this.root)

    items.forEach((item) => {
      const view = this.createItemView()

      view.content = item

      this.root.append(view.root)
    })
  }
}
