import { OptionItem } from './content'

export class QuestionOptionView {
  listener: null | (() => void)

  readonly root = document.createElement('div')
  private readonly input = document.createElement('input')
  private readonly label = document.createElement('label')
  private readonly labelText = document.createElement('span')
  private readonly image = document.createElement('img')

  constructor(questionIndex: number, answerIndex: number) {
    this.root.classList.add('question-option')

    this.input.type = 'radio'
    this.input.id = 'question_' + questionIndex + '_' + answerIndex
    this.input.name = 'question_' + questionIndex
    this.input.value = answerIndex.toString(16)

    this.label.htmlFor = this.input.id

    this.label.append(this.labelText, this.image)
    this.root.append(this.input, this.label)

    this.input.addEventListener('change', () => {
      if (this.listener) this.listener()
    })
  }

  set item(item: OptionItem) {
    this.labelText.innerText = item.text

    if (item.image !== undefined) {
      this.image.src = item.image
      this.image.classList.remove('hide')
    } else {
      this.image.src = ''
      this.image.classList.add('hide')
    }
  }

  set selected(value: boolean) { this.input.checked = value }
}
